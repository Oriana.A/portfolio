# Projet portfolio

15/07/2109 - 30/07/2019 
Oriana A

Premier projet de ma formation "developpeur web et web mobile" à Simplon. Le portfolio sera visible sur le site simplonlyon.fr . 

Le portfolio contient : 

- une présentation

- une liste de mes projets

- un contact

- un lien vers mon gitlab

- mes langages/technologie préférées

  


## Conception

Le projet devra avoir : 

- 3 - 4 users stories ou plus de votre site portfolio (histoire de définir qui utilisera le site et pourquoi)
- La ou les maquettes fonctionnelles du site (qui doivent en gros indiquée les éléments d'interface et leur positionnements)
- Un projet gitlab avec un README qui décrit le projet + users stories et maquette fonctionnelle
- Faire la structure HTML complète de l'application avant de commencer le style



## Maquette

**Version 1**


![version 1](maquettes/maquette v1.jpg)




**Version 2 + mobile**

**[version interactive mobile](https://marvelapp.com/4d37a4i)**

![version 2](maquettes/Maquette v2.jpg)





## Users stories

> En tant que recruteur, je souhaite avoir un accès facile aux projets de mon potentiel employé afin de pouvoir me rendre compte de ses compétences.



> En tant que recruteur, j'aimerais avoir un espace de présentation de la personne afin de me faire un premier avis sur son parcours et sa personnalité.



> En tant que potentiel client, je souhaite pouvoir avoir un espace d'envoie de message afin de contacter facilement et rapidement la personne à qui je souhaite confier un projet.















